package soundwave.com.example;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.Soundwave;

public class MainActivity extends AppCompatActivity {

    private Soundwave soundwave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById(R.id.transmit_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, Main2Activity.class));
            }
        });
        soundwave = new Soundwave(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        soundwave.listen(new Soundwave.Listener() {
            @Override
            public void decoded(String value) {
                ((TextView) findViewById(R.id.listen_value)).setText(value);
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        soundwave.stopReceiver();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        soundwave.destroy();
    }
}
