package com;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.util.Log;

import com.libra.sinvoice.Common;
import com.libra.sinvoice.SinVoicePlayer;
import com.libra.sinvoice.SinVoiceRecognition;

import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;

public class Soundwave implements SinVoiceRecognition.Listener {

    private final static int MSG_SET_RECG_TEXT = 1;
    private final static int MSG_RECG_START = 2;
    private final static int MSG_RECG_END = 3;
    private final static int MSG_PLAY_TEXT = 4;

    private WeakReference<Context> context;
    private Handler mHanlder;
    private SinVoicePlayer transmitter;
    private SinVoiceRecognition receiver;
    private char mRecgs[] = new char[100];
    private int mRecgCount;

    private Listener listener = new Listener() {
        @Override
        public void decoded(String value) {

        }
    };

    private final static int[] TOKENS = {32, 32, 32, 32, 32, 32};
    private final static int TOKEN_LEN = TOKENS.length;

    public Soundwave(Context context) {
        System.loadLibrary("sinvoice");
        this.context = new WeakReference<>(context);
        transmitter = new SinVoicePlayer();
        receiver = new SinVoiceRecognition();
        transmitter.init(this.context.get());
        receiver.init(this.context.get());
        receiver.setListener(this);
        mHanlder = new RegHandler(this);
    }

    public void listen(Listener listener) {
        this.listener = listener;
        receiver.start(TOKEN_LEN, false);
    }

    public void transmit(@NonNull String message) {
        String value = message;
        if (message.length() > 25) {
            value = message.substring(0, 25);
        }
        value = value.replace("|", " ");

        try {
            byte[] strs = value.getBytes("UTF8");
            int len = strs.length;
            int[] tokens = new int[len];
            int maxEncoderIndex = transmitter.getMaxEncoderIndex();
            for (int i = 0; i < len; ++i) {
                if (maxEncoderIndex < 255) {
                    tokens[i] = Common.DEFAULT_CODE_BOOK.indexOf(value.charAt(i));
                } else {
                    tokens[i] = strs[i];
                }
            }
            transmitter.play(tokens, len, false, 2000);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public void stopTransmitter() {
        transmitter.stop();
    }

    public void stopReceiver() {
        receiver.stop();
    }

    public void destroy() {
        stopReceiver();
        stopTransmitter();
        receiver.uninit();
        transmitter.uninit();
    }

    protected Context getContext() {
        return this.context.get();
    }

    @Override
    public void onSinVoiceRecognitionStart() {
        mHanlder.sendEmptyMessage(MSG_RECG_START);
    }

    @Override
    public void onSinVoiceRecognition(char ch) {
        mHanlder.sendMessage(mHanlder.obtainMessage(MSG_SET_RECG_TEXT, ch, 0));
    }

    @Override
    public void onSinVoiceRecognitionEnd(int result) {
        mHanlder.sendMessage(mHanlder.obtainMessage(MSG_RECG_END, result, 0));
    }

    private static class RegHandler extends Handler {
        private Soundwave sin;

        public RegHandler(Soundwave sin) {
            this.sin = sin;
        }

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_SET_RECG_TEXT:
                    char ch = (char) msg.arg1;
                    sin.mRecgs[sin.mRecgCount++] = ch;
                    break;

                case MSG_RECG_START:
                    sin.mRecgCount = 0;
                    break;

                case MSG_RECG_END:
                    if (sin.mRecgCount > 0) {
                        byte[] strs = new byte[sin.mRecgCount];
                        for (int i = 0; i < sin.mRecgCount; ++i) {
                            strs[i] = (byte) sin.mRecgs[i];
                        }
                        try {
                            String strReg = new String(strs, "UTF8");
                            strReg = strReg.replace("|", " ");
                            if (msg.arg1 >= 0) {
                                if (null != sin) {
                                    sin.listener.decoded(strReg);
                                }
                            } else {
                                sin.listener.decoded(strReg);
                            }
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                    break;

                case MSG_PLAY_TEXT:
                    break;
            }
            super.handleMessage(msg);
        }
    }


    public interface Listener {
        void decoded(String value);
    }

}
